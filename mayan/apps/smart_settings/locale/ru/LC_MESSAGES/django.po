# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# lilo.panic, 2016
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-02 14:32-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Sergey Glita <gsv70@mail.ru>\n"
"Language-Team: Russian (http://www.transifex.com/rosarior/mayan-edms/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: apps.py:21 permissions.py:7
msgid "Smart settings"
msgstr "Гибкие настройки"

#: apps.py:29
msgid "Setting count"
msgstr "Количество настроек"

#: apps.py:33
msgid "Name"
msgstr "Имя"

#: apps.py:37
msgid "Value"
msgstr "Значение"

#: apps.py:40
msgid "Overrided by environment variable?"
msgstr ""

#: apps.py:41
msgid "Yes"
msgstr "Да"

#: apps.py:41
msgid "No"
msgstr "Нет"

#: forms.py:12
msgid "Enter the new setting value."
msgstr ""

#: forms.py:31
msgid "Value must be properly quoted."
msgstr ""

#: forms.py:40
#, python-format
msgid "\"%s\" not a valid entry."
msgstr ""

#: links.py:12 links.py:16
msgid "Settings"
msgstr "Настройки"

#: links.py:21
msgid "Namespaces"
msgstr ""

#: links.py:25
msgid "Edit"
msgstr "Редактировать"

#: permissions.py:10
msgid "Edit settings"
msgstr ""

#: permissions.py:13
msgid "View settings"
msgstr "Просмотр настроек"

#: views.py:18
msgid "Setting namespaces"
msgstr "Пространство имён параметра"

#: views.py:34
msgid ""
"Settings inherited from an environment variable take precedence and cannot "
"be changed in this view. "
msgstr ""

#: views.py:37
#, python-format
msgid "Settings in namespace: %s"
msgstr "Параметры в пространстве имён: %s"

#: views.py:45
#, python-format
msgid "Namespace: %s, not found"
msgstr "Пространство имён: %s, не найдено"

#: views.py:60
msgid "Setting updated successfully."
msgstr ""

#: views.py:68
#, python-format
msgid "Edit setting: %s"
msgstr ""
